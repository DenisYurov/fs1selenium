/*fsone1inc.com web pages test
* denis yurov
*/
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class Fs1Selenium {
     static ChromeDriver driver = null;
     //static String urlForCheck = "https://zzzzzz.ga/";
    static String urlForCheck = "https://www.fs1inc.com/";
   //  static String urlForCheck ="https://35.231.99.175/";


    @BeforeClass
    static public void beforetest() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\FS1 Web User\\IdeaProjects\\automated testing\\chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @AfterClass
    static public void aftertest() {
        driver.quit();
    }

    public boolean isElementPresent(By selector) {
        return driver.findElements(selector).size() > 0;
    }

    // Home Page
    @Test
    public void homePage() throws IOException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // I dont know why webdriver won't go to home page directly
        String urChk = this.urlForCheck + "ford-lincoln-mercury.html";
        driver.get(urChk);
        urChk = this.urlForCheck;
        driver.get(urChk);

        wait.until(ExpectedConditions.titleContains("Flagship One Inc | PCM | ECM | ECU | Engine Computer"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"search_mini_form\"]/div/button")));
        assertTrue("Verification Failed: no Search button!", isElementPresent(By.xpath("//*[@id=\"search_mini_form\"]/div/button")));
        assertTrue("Verification Failed: no Search input field!", isElementPresent(By.xpath("//*[@id=\"search\"]")));
        assertTrue("Verification Failed: no Slideshow!", isElementPresent(By.xpath("//*[@id=\"ts_1975\"]")));
        assertTrue("Verification Failed: no Products on home page!", isElementPresent(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[3]/div/div/div[2]/ul/ul/div/ul[1]/li[1]/div[2]/button")));
        assertTrue("Verification Failed: no Phone number on top of page!", isElementPresent(By.xpath("//*[@id=\"header_mw\"]/div/div/h1/span")));
        assertTrue("Verification Failed: no Second category menu item on top of page!", isElementPresent(By.xpath("//*[@id=\"menu\"]/li[2]/a/span")));
        assertTrue("Verification Failed: no Third category menu item on top of page!", isElementPresent(By.cssSelector(".level0.nav-2.level-top")));

    }
    // Home Page
    // search  by product name.
    @Test
    public void serchFromHomePage() throws IOException{
        WebDriverWait wait = new WebDriverWait(driver, 10);
        String urChk = this.urlForCheck;
        driver.get(urChk);
        // taken first top left product on home page
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated( By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[3]/div/div/div[2]/ul/ul/div/ul[1]/li[1]/h2/a")));
        // get name of product from link text
        String attributeValue = element.getAttribute("text");
        // fill out search field
        element = wait.until(ExpectedConditions.presenceOfElementLocated( By.xpath("//*[@id=\"search\"]" )));
        element.sendKeys(attributeValue);

        element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"search_mini_form\"]/div/button")));
        element.click();
        wait.withMessage("Add to cart button not found after product search")
            .until(ExpectedConditions.elementToBeClickable( By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[2]/ul/li/div[2]/button")));
    }

    @Test //ford-lincoln-mercury Page
    public void fordLincolnMegcuryPage() throws IOException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        String urChk = this.urlForCheck + "ford-lincoln-mercury.html";
        driver.get(urChk);
        wait.until(ExpectedConditions.titleContains("Ford | Lincoln | Mercury | Engine Computers | ECU | ECM | PCM"));
        //search field
        assertTrue("Page ford-lincoln-mercury.1: no \"Search\" field on the top of page", isElementPresent(By.xpath("//*[@id=\"search\"]")));
        assertTrue("Page ford-lincoln-mercury.2: no meny item \"Make\" in left column menu!", isElementPresent(By.xpath("//*[@id=\"narrow-by-list\"]/dt[1]")));
        assertTrue("Page ford-lincoln-mercury.3: no sub meny  item  \"Ford\" in left column menu!", isElementPresent(By.xpath("//*[@id=\"narrow-by-list\"]/dd[1]/ol/li[1]/a")));
        //going to open menu Model
        WebElement modelMenuItem = driver.findElement(By.xpath("//*[@id=\"narrow-by-list\"]/dt[2]"));
        modelMenuItem.click();
        assertTrue("Page ford-lincoln-mercury.4: no sub meny item  \"Model -> aerostar\" in left column menu!", isElementPresent(By.xpath("//*[@id=\"narrow-by-list\"]/dd[2]/ol/li[1]/a")));
        assertTrue("Page ford-lincoln-mercury.5: no meny  item \"Engine Type\"in left column menu!", isElementPresent(By.xpath("//*[@id=\"narrow-by-list\"]/dt[5]")));
        assertTrue("Page ford-lincoln-mercury.6: no Slide show  on top of page!", isElementPresent(By.xpath("//*[@id=\"camera_wrap_1\"]/div[1]/div[4]/div/div[1]/a")));
        WebElement pageLimiter = driver.findElement(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[4]/div[2]/div/div[2]/div[2]"));
        // Page limiter
        String str = "<div class=\"pages\"><strong>Page:</strong><ol><li class=\"current\">1</li><li><a href=\""+urlForCheck+"ford-lincoln-mercury.html?p=2\">2</a></li><li><a href=\""+urlForCheck+"ford-lincoln-mercury.html?p=3\">3</a></li>\n" +
                     "<li><a href=\""+urlForCheck+"ford-lincoln-mercury.html?p=4\">4</a></li><li><a href=\""+urlForCheck+"ford-lincoln-mercury.html?p=5\">5</a></li><li><a class=\"next i-next\" href=\""+urlForCheck+"ford-lincoln-mercury.html?p=2\" title=\"Next\">\n" +
                     "Next</a></li></ol></div>";

        String str1 = pageLimiter.getAttribute("outerHTML");
        assertEquals("Page ford-lincoln-mercury.7: something wrong with page limiter. Page bottom",str.replaceAll("\\s+","") ,str1.replaceAll("\\s+",""));
        //Footer
       // this.footer(urChk);
/*
        WebElement footer = driver.findElement(By.xpath("//*[@id=\"footer_mw\"]"));
        str1 = footer.getAttribute("outerHTML");
        assertEquals("Page ford-lincoln-mercury.8: something wrong with page footer",this.pageFooterHtml.replaceAll("\\s+","") ,str1.replaceAll("\\s+",""));
*/
    }
    //Page footer
    public void footer(String urChk) throws IOException{
        //Footer

        String urlForChecknoLast = removeLast(urlForCheck);
        System.out.println(urlForChecknoLast);
        String pageFooterHtml="<footerid=\"footer_mw\"><divclass=\"footer-container\"><divclass=\"footer\"><divclass=\"box_links_footer\"><divclass=\"left\"><ul><li><ahref=\""+urlForChecknoLast+"\">Home</a></li><li><ahref=\""+urlForChecknoLast+"/about-us\">AboutUs</a></li><li><atitle=\"FlagshipOne-Blog\"href=\""+urlForCheck+"blog/\"target=\"_blank\">Blog</a></li><liclass=\"footcontact\"><ahref=\""+urlForCheck+"contacts\">ContactUs</a></li><li><ahref=\""+urlForCheck+"customer-service\">CustomerService</a></li><liclass=\"footprogram\"><ahref=\""+urlForCheck+"programming\">Programming</a></li><li><ahref=\""+urlForCheck+"privacy-policy-cookie-restriction-mode\">PrivacyPolicy</a></li><li><ahref=\""+urlForCheck+"customer-testimonials\">Reviews</a></li><li><ahref=\""+urlForCheck+"faq\">FAQ</a></li></ul><ulclass=\"links\"><liclass=\"firstlast\"><ahref=\""+urlForCheck+"catalog/seo_sitemap/category/\"title=\"SiteMap\">SiteMap</a></li></ul></div><divclass=\"icfooter\"><divclass=\"right\"><ulclass=\"icon_links\"><liclass=\"icon_go\"><iclass=\"fafa-twitter\"aria-hidden=\"true\"></i><ahref=\"https://www.twitter.com/theflagshipone/\"onclick=\"this.target='_blank'\">Twitter</a></li><liclass=\"icon_fb\"><iclass=\"fafa-facebook\"aria-hidden=\"true\"></i><ahref=\"https://www.facebook.com/flagshipone\"onclick=\"this.target='_blank'\">Facebook</a></li><liclass=\"icon_tw\"><iclass=\"fafa-instagram\"aria-hidden=\"true\"></i><ahref=\"https://www.instagram.com/flagship_one\"onclick=\"this.target='_blank'\">Instagram</a></li></ul></div></div><divclass=\"clear_b\">&nbsp;</div></div><divclass=\"box_credit_footer\"><divclass=\"left\"><!--BBBLogo--><aclass=\"bbb-logo\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.bbb.org/new-york-city/business-reviews/auto-parts-and-supplies-used-and-rebuilt/flagship-one-inc-in-east-rockaway-ny-136557/#bbbonlineclick','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"></a><!--(c)2005,2015.Authorize.NetisaregisteredtrademarkofCyberSourceCorporation--><aclass=\"authorize-net\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://verify.authorize.net/anetseal/?pid=8b69585b-6936-4e10-aae3-9da4b0714569&amp;rurl=http%3A//www.fs1inc.com/','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=960,height=400');returnfalse;\"></a><!--PayPalLogo--><aclass=\"paypal\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"></a><divclass=\"paypal-nodisplay\"><ahref=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"><imgtitle=\"PaypalVerified\"src=\"/skin/frontend/default/mEbay/images/paypal-verified.png\"width=\"90\"height=\"72\"alt=\"FlagshipOne-PaypalVerified\"></a></div><!--PayPalLogo--></div><divclass=\"right\"><ul><li><iclass=\"fafa-cc-mastercard\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-visa\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-amex\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-discover\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-paypal\"style=\"font-size:30px\"></i></li></ul><divclass=\"godaddy\"><imgsrc=\"https://uspsblog.com/wp-content/uploads/2015/11/USPS_Eagle-Symbol-web-size.png\"width=\"38\"height=\"auto\"alt=\"FreeUnitedStatesPostalServiceShipping\"><imgsrc=\"/skin/frontend/default/mEbay/images/UPS-USPS.png\"width=\"23\"height=\"auto\"alt=\"FreeUnitedStatesPostalServiceShipping\"><imgsrc=\"/skin/frontend/default/mEbay/images/GoDaddySiteSeal.png\"width=\"131\"height=\"32\"onclick=\"verifySeal();\"alt=\"GoDaddySSLVerification\"></div></div><divclass=\"center\"itemscope=\"\"itemtype=\"https://schema.org/LocalBusiness\"><divitemprop=\"address\"itemscope=\"\"itemtype=\"https://schema.org/PostalAddress\"><pclass=\"fsaddress2\"><spanitemprop=\"streetAddress\">21RyderPl.</span><spanitemprop=\"addresslocality\">EastRockaway</span>,<spanitemprop=\"addressRegion\">NY</span><spanitemprop=\"postalCode\">11518</span></p></div><pclass=\"fsaddress3\"><aclass=\"gmail\"href=\"mailto:admin@fs1inc.com\">admin@fs1inc.com</a><spanclass=\"calltracking\">516-766-2223</span></p><pclass=\"fsaddress4\"><aclass=\"subscribe\"href=\"https://eepurl.com/deNEEL\">SubscribeToFS1INC</a></p></div><pclass=\"fsaddress\">©2018<spanitemprop=\"name\">FlagshipOne,Inc.</span>AllRightsReserved.</p></div></div></div></footer>";
        driver.get(urChk);
        WebElement footer = driver.findElement(By.xpath("//*[@id=\"footer_mw\"]"));
        String str1 = footer.getAttribute("outerHTML");
        assertEquals("Page ford-lincoln-mercury.8: something wrong with page footer",pageFooterHtml.replaceAll("\\s+","") ,str1.replaceAll("\\s+",""));

    };
    //static String pageFooterHtml="<footerid=\"footer_mw\"><divclass=\"footer-container\"><divclass=\"footer\"><divclass=\"box_links_footer\"><divclass=\"left\"><ul><li><ahref=\""+urlForCheck+"\">Home</a></li><li><ahref=\""+urlForCheck+"/about-us\">AboutUs</a></li><li><atitle=\"FlagshipOne-Blog\"href=\""+urlForCheck+"/blog/\"target=\"_blank\">Blog</a></li><liclass=\"footcontact\"><ahref=\""+urlForCheck+"/contacts\">ContactUs</a></li><li><ahref=\""+urlForCheck+"/customer-service\">CustomerService</a></li><liclass=\"footprogram\"><ahref=\""+urlForCheck+"/programming\">Programming</a></li><li><ahref=\""+urlForCheck+"/privacy-policy-cookie-restriction-mode\">PrivacyPolicy</a></li><li><ahref=\""+urlForCheck+"/customer-testimonials\">Reviews</a></li><li><ahref=\""+urlForCheck+"/faq\">FAQ</a></li></ul><ulclass=\"links\"><liclass=\"firstlast\"><ahref=\""+urlForCheck+"/catalog/seo_sitemap/category/\"title=\"SiteMap\">SiteMap</a></li></ul></div><divclass=\"icfooter\"><divclass=\"right\"><ulclass=\"icon_links\"><liclass=\"icon_go\"><iclass=\"fafa-twitter\"aria-hidden=\"true\"></i><ahref=\"https://www.twitter.com/theflagshipone/\"onclick=\"this.target='_blank'\">Twitter</a></li><liclass=\"icon_fb\"><iclass=\"fafa-facebook\"aria-hidden=\"true\"></i><ahref=\"https://www.facebook.com/flagshipone\"onclick=\"this.target='_blank'\">Facebook</a></li><liclass=\"icon_tw\"><iclass=\"fafa-instagram\"aria-hidden=\"true\"></i><ahref=\"https://www.instagram.com/flagship_one\"onclick=\"this.target='_blank'\">Instagram</a></li></ul></div></div><divclass=\"clear_b\">&nbsp;</div></div><divclass=\"box_credit_footer\"><divclass=\"left\"><!--BBBLogo--><aclass=\"bbb-logo\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.bbb.org/new-york-city/business-reviews/auto-parts-and-supplies-used-and-rebuilt/flagship-one-inc-in-east-rockaway-ny-136557/#bbbonlineclick','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"></a><!--(c)2005,2015.Authorize.NetisaregisteredtrademarkofCyberSourceCorporation--><aclass=\"authorize-net\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://verify.authorize.net/anetseal/?pid=8b69585b-6936-4e10-aae3-9da4b0714569&amp;rurl=http%3A//www.fs1inc.com/','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=960,height=400');returnfalse;\"></a><!--PayPalLogo--><aclass=\"paypal\"href=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"></a><divclass=\"paypal-nodisplay\"><ahref=\"https://www.paypal.com/webapps/mpp/paypal-popup\"title=\"HowPayPalWorks\"onclick=\"javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1060,height=700');returnfalse;\"><imgtitle=\"PaypalVerified\"src=\"/skin/frontend/default/mEbay/images/paypal-verified.png\"width=\"90\"height=\"72\"alt=\"FlagshipOne-PaypalVerified\"></a></div><!--PayPalLogo--></div><divclass=\"right\"><ul><li><iclass=\"fafa-cc-mastercard\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-visa\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-amex\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-discover\"style=\"font-size:30px\"></i></li><li><iclass=\"fafa-cc-paypal\"style=\"font-size:30px\"></i></li></ul><divclass=\"godaddy\"><imgsrc=\"https://uspsblog.com/wp-content/uploads/2015/11/USPS_Eagle-Symbol-web-size.png\"width=\"38\"height=\"auto\"alt=\"FreeUnitedStatesPostalServiceShipping\"><imgsrc=\"/skin/frontend/default/mEbay/images/UPS-USPS.png\"width=\"23\"height=\"auto\"alt=\"FreeUnitedStatesPostalServiceShipping\"><imgsrc=\"/skin/frontend/default/mEbay/images/GoDaddySiteSeal.png\"width=\"131\"height=\"32\"onclick=\"verifySeal();\"alt=\"GoDaddySSLVerification\"></div></div><divclass=\"center\"itemscope=\"\"itemtype=\"https://schema.org/LocalBusiness\"><divitemprop=\"address\"itemscope=\"\"itemtype=\"https://schema.org/PostalAddress\"><pclass=\"fsaddress2\"><spanitemprop=\"streetAddress\">21RyderPl.</span><spanitemprop=\"addresslocality\">EastRockaway</span>,<spanitemprop=\"addressRegion\">NY</span><spanitemprop=\"postalCode\">11518</span></p></div><pclass=\"fsaddress3\"><aclass=\"gmail\"href=\"mailto:admin@fs1inc.com\">admin@fs1inc.com</a><spanclass=\"calltracking\">516-766-2223</span></p><pclass=\"fsaddress4\"><aclass=\"subscribe\"href=\"https://eepurl.com/deNEEL\">SubscribeToFS1INC</a></p></div><pclass=\"fsaddress\">©2018<spanitemprop=\"name\">FlagshipOne,Inc.</span>AllRightsReserved.</p></div></div></div></footer>";
    public String removeLast(String str) {
                    str = str.replace(str.substring(str.length()-1), "");
            return str;

    }
    @Test //Search ford-lincoln-mercury Page
    public void fordLincolnMegcuryPageSearch() throws IOException {
        String urChk = this.urlForCheck + "ford-lincoln-mercury.html";
        this.driver.get(urChk);
        System.out.println("fordLincolnMegcuryPageSearch " + driver.getCurrentUrl());
        assertTrue("Verification Failed: no \"Search\" button on the top of page", isElementPresent(By.xpath("//*[@id=\"search_mini_form\"]/div/button")));
        WebElement searchField = driver.findElement(By.xpath("//*[@id=\"search\"]"));
        searchField.sendKeys("Jeep");
        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"search_mini_form\"]/div/button"));
        searchButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[2]/ul[1]/li[1]/div[2]/button"))));
        assertTrue("Verification Failed: First \"add to cart\" button",isElementPresent(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[2]/ul[1]/li[1]/div[2]/button")));
    }

    //checkout test. From lincoln page to checkout.
    @Test //Search ford-lincoln-mercury Page
    public void fordLincolnMegcuryPageCart() throws IOException {
        this.driver.get(this.urlForCheck + "ford-lincoln-mercury.html");
        System.out.println(driver.getCurrentUrl());
        WebDriverWait wait = new WebDriverWait(driver, 20);
        Select dropdown;
        //take first add to cart button
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[4]/ul[1]/li[1]/div[2]/button")));
                                                                        //*[@id="content_mw"]/div/div/div/div[5]/ul[1]/li[1]/div[2]/button
        WebElement addToCart = driver.findElement(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div[4]/ul[1]/li[1]/div[2]/button"));
        addToCart.click();
        //chechking Shopping Cart element under top menu
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[1]")));
        Assert.assertEquals("Failed! \"Add to Cart\" button on ford-lincoln-mercury.html", "Shopping Cart", driver.getTitle());
        //press proceedToCheckout
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[2]/div[2]/ul/li[1]/button")));
        assertTrue("Failed! no \"Proceed to Check Out\" button",isElementPresent(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[2]/div[2]/ul/li[1]/button")));

        WebElement proceedToCheckout = driver.findElement(By.xpath("//*[@id=\"content_mw\"]/div/div/div/div/div[2]/div[2]/ul/li[1]/button"));
        proceedToCheckout.click();
        boolean isTitleCorrect = wait.until
                (ExpectedConditions .
                        titleIs("Checkout"));
        assertTrue("Failed! page title have to be \"Checkout\"", isTitleCorrect == true);
        //page Checkout
        //field First Name
        assertTrue("Failed! field \"First Name\" missing",isElementPresent(By.xpath("//*[@id=\"billing:firstname\"]")));
        WebElement element = driver.findElement(By.xpath("//*[@id=\"billing:firstname\"]"));
        element.sendKeys("Test");
        //field Last Name
        assertTrue("Failed! field \"Last Name\" missing",isElementPresent(By.xpath("//*[@id=\"billing:lastname\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:lastname\"]"));
        element.sendKeys("Test");
        //field Email Address
        assertTrue("Failed! field \"Email Address\" missing",isElementPresent(By.xpath("//*[@id=\"billing:email\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:email\"]"));
        element.sendKeys("test@mail.ru");
        //Confirm Email Address
        /* no on live
        assertTrue("Failed! field \"Confirm Email Address\" missing",isElementPresent(By.xpath("//*[@id=\"confirmation3\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"confirmation3\"]"));
        element.sendKeys("test@mail.ru");
        */

       //field Address
        assertTrue("Failed! field \"Address\" missing",isElementPresent(By.xpath("//*[@id=\"billing:street1\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:street1\"]"));
        element.sendKeys("3000 brighton 12th street apt D7");
        // City
        assertTrue("Failed! field \"Address\" missing",isElementPresent(By.xpath("//*[@id=\"billing:city\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:city\"]"));
        element.sendKeys("Brooklyn");
        //zip
        assertTrue("Failed! field \"Zip\" missing",isElementPresent(By.xpath("//*[@id=\"billing:postcode\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:postcode\"]"));
        element.sendKeys("11235");

        // region
        assertTrue("Failed! field \"Region dropdown\" missing",isElementPresent(By.xpath("//*[@id=\"billing:region_id\"]")));
        assertTrue("Failed! Region dropdown should be displayed",driver.findElement(By.xpath("//*[@id=\"billing:region_id\"]")).isDisplayed());
        element = driver.findElement(By.xpath("//*[@id=\"billing:region_id\"]"));
        dropdown = new Select(element);
        dropdown.selectByVisibleText("New York");

        //country
        assertTrue("Failed! field \"Country dropdown\" missing",isElementPresent(By.xpath("//*[@id=\"billing:country_id\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"billing:country_id\"]"));
        dropdown = new Select(element);
        dropdown.selectByVisibleText("United States");
        //phone
        assertTrue("Failed! field \"Telephone\" missing",isElementPresent(By.xpath("//*[@id=\"billing:telephone\"]")));
        driver.findElement(By.xpath("//*[@id=\"billing:telephone\"]")).sendKeys("1123567");
        //paypal
        assertTrue("Failed! field \"Paypal radio button\" missing",isElementPresent(By.xpath("//*[@id=\"p_method_paypal_standard\"]")));
        driver.findElement(By.xpath("//*[@id=\"p_method_paypal_standard\"]")).click();
        //press Place Order Button
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"aitcheckout-place-order\"]")));
        driver.findElement(By.xpath("//*[@id=\"aitcheckout-place-order\"]")).click();
        wait.until(ExpectedConditions.titleContains("PayPal"));
        Assert.assertTrue("Failed! not paypal page. Actual page is:" + driver.getCurrentUrl(),driver.getCurrentUrl().contains("www.paypal.com"));
        //System.out.println(driver.getTitle());
    }

    //Page Contact us
    @Test
    public void contactPage() throws IOException {
        this.driver.get(this.urlForCheck + "contacts/index/");
        System.out.println(driver.getCurrentUrl());
        WebDriverWait wait = new WebDriverWait(driver, 20);
        //take submit button
        wait.until(ExpectedConditions.presenceOfElementLocated (By.xpath("//*[@id=\"contactsSubmit\"]")));

        assertTrue("field \"NAME\" missing",isElementPresent(By.xpath("//*[@id=\"name2\"]")));
        WebElement element = driver.findElement(By.xpath("//*[@id=\"name2\"]"));
        element.sendKeys("TestName");

        assertTrue("email \"NAME\" missing",isElementPresent(By.xpath("//*[@id=\"email2\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"email2\"]"));
        element.sendKeys("test@test.com");

        assertTrue("email \"TELEPHONE\" missing",isElementPresent(By.xpath("//*[@id=\"telephone2\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"telephone2\"]"));
        element.sendKeys("123456");

        assertTrue("email \"COMMENT\" missing",isElementPresent(By.xpath("//*[@id=\"comment\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"comment\"]"));
        element.sendKeys("Test Comment");

        assertTrue("email \"COMMENT\" missing",isElementPresent(By.xpath("//*[@id=\"comment\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"comment\"]"));
        element.sendKeys("Test Comment");

        //click button send
        WebElement proceedToSendContact = driver.findElement(By.xpath("//*[@id=\"contactsSubmit\"]"));
        proceedToSendContact.click();
        wait.until(ExpectedConditions.titleIs("Contact Us"));
        assertTrue("email \"SUBMIT\" button is not work correctly",isElementPresent(By.xpath("//*[@id=\"messages_product_view\"]/ul/li/ul/li/span")));

    }

}
